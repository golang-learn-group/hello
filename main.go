package main

import (
	"aniruddha-dev/functions/auxfunctions"
	"aniruddha-dev/functions/datatypes/arithmeticOps"
	"aniruddha-dev/functions/datatypes/pointers"
	"aniruddha-dev/functions/flowcontrol/conditionalFlowControl"
	"aniruddha-dev/functions/flowcontrol/conditionalFlowControl/switchcase"
	"aniruddha-dev/functions/flowcontrol/loopconstruct"
	"aniruddha-dev/functions/patternprinting"
	"fmt"
)

func pointersDemo() {
	a, b := 2, 4
	fmt.Printf("Original Values -> a = %d, b = %d \n", a, b)
	c, d := pointers.Swap(&a, &b)
	fmt.Printf("Swapped Values -> c = %d, d = %d \n", c, d)
}

func flowControlDemo() {
	conditionalFlowControl.SimpleIfStatement()
	conditionalFlowControl.SimpleIfElseStatement()
	conditionalFlowControl.SimpleIfElseAndElseStatement()
	conditionalFlowControl.UnderstandingIfElseStatementScope()
	conditionalFlowControl.ConvertStringToFloat()
}

func loopDemo() {
	loopconstruct.ConstructingForLoop()
	loopconstruct.BasicForLoop()
	loopconstruct.BasicEnumeratingSequencesDemo()
	loopconstruct.EnumeratingStringIntoCharactersDemo()
}

func switchCaseDemo() {
	switchcase.BasicSwitchCase()
}

func patternPrinting() {
	auxfunctions.Linesfeed("Square Pattern")
	patternprinting.SolidRectangle()
	auxfunctions.Linesfeed("Hollow Square Pattern")
	patternprinting.HollowRectangle()
	auxfunctions.Linesfeed("Half Pyramid")
	patternprinting.HalfPyramid()
	auxfunctions.Linesfeed("Inverted Half Pyramid")
	patternprinting.InvertedHalfPyramid()
	auxfunctions.Linesfeed("Hollow Inverted Half Pyramid")
	patternprinting.HollowInvertedHalfPyramid()
}

func main() {
	auxfunctions.Linesfeed("Golang learning and code scratchpads\n\r 1. Golang Pointers")
	pointersDemo()
	auxfunctions.Linesfeed("pointing at pointers ...")
	pointers.PointingAtPointers()
	auxfunctions.Linesfeed("Usefulness of pointers by using a string array sorting Demo . . .")
	pointers.StringSortExample()
	auxfunctions.Linesfeed("unary operations ...")
	arithmeticOps.UnaryOpsDemo()
	auxfunctions.Linesfeed("")
	flowControlDemo()
	auxfunctions.Linesfeed("")
	loopDemo()
	auxfunctions.Linesfeed("")
	switchCaseDemo()

	auxfunctions.Linesfeed("Pattern Printing Problems")
	patternPrinting()
}
