package patternprinting

import (
	"fmt"
)

//solid rectangle
/**
	*       *       *       *       *
	*       *       *       *       *
	*       *       *       *       *
	*       *       *       *       *
	*       *       *       *       *
**/
func SolidRectangle() {
	size := 5

	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			fmt.Print("*\t")
		}
		fmt.Println()
	}
}

/*
* Hollow Rectangle **

  - *       *       *       *
  - *
  - *
  - *
  - *       *       *       *

*
*/
func HollowRectangle() {
	size := 5
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			if i == size-1 || j == size-1 || i == 0 || j == 0 {
				fmt.Print("*\t")
			} else {
				fmt.Print("\t")
			}
		}
		fmt.Println()
	}
}

/*
* HALF PYRAMID
	*
	*       *
	*       *       *
	*       *       *       *
	*       *       *       *       *
*
*/
func HalfPyramid() {
	size := 5
	for i := 0; i < size; i++ {
		for j := 0; j <= i; j++ {
			fmt.Print("*\t")
		}
		fmt.Println()
	}
}

/*
* Inverted Half Pyramid

  - *       *       *       *	   *

  - *       *       *       *

  - *       *       *

  - *       *
    *

    00 01 02 03 04 5
    10 11 12 13    4
    20 21 22       3
    30 31          2
    40             1

Hint: reduce the number of * with the increase in i value; therefore size - i
*/
func InvertedHalfPyramid() {
	size := 5
	for i := 0; i < size; i++ {
		for j := 0; j < size-i; j++ {
			fmt.Print("*\t")
		}
		fmt.Println()
	}
}

/*
* Hollow Inverted Half Pyramid

  - *       *       *       *	   *

  - *                       *

  - *               *

  - *       *
    *

    00 01 02 03 04 5
    10 11 12 13    4
    20 21 22       3
    30 31          2
    40             1

Hint: reduce the number of * with the increase in i value; therefore size - i

in hollow inverted half pyramid remove 11 12

	21

TODO : SOlve this . . .
*/
func HollowInvertedHalfPyramid() {
	size := 5
	for i := 1; i <= size; i++ {
		for j := i; j <= size; j++ {
			if i == 1 || j == i || j == size {
				fmt.Print("*\t")
			} else {
				fmt.Print("\t")
			}
		}
		fmt.Println()
	}
}
