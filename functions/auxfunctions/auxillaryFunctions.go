package auxfunctions

import "fmt"

func PrintStringArrayContents(arr []string) {
	for _, elem := range arr {
		fmt.Print(elem, ",")
	}

	fmt.Println()
}

func Linesfeed(arg string) {
	fmt.Println()
	fmt.Println(arg)
}
