package arithmeticOps

import "fmt"

func UnaryOpsDemo() {
	a := 2
	fmt.Println("a before ++ increment ", a)
	a++
	a += 2
	a--
	a -= 2
	fmt.Println("After ++, +=, --, -= , the value of a is -> ", a)
}
