package pointers

import (
	"aniruddha-dev/functions/auxfunctions"
	"fmt"
	"sort"
)

func Swap(a, b *int) (int, int) {
	tmp := *a
	*a = *b
	*b = tmp

	return *a, *b
}

func PointingAtPointers() {
	first := 100
	second := &first
	third := &second
	fmt.Println(first)
	fmt.Println(*second)
	fmt.Println(**third)
}

func StringSortExample() {
	names := []string{"Sebin", "Aniruddha", "Lalit", "Bibin"}
	fmt.Println("Initial content of names array")
	auxfunctions.PrintStringArrayContents(names)
	secondName := &names[1]
	fmt.Println("Name at the 1st position (starting from 0) -> ", *secondName)
	sort.Strings(names[:])
	fmt.Println("post sorting the string array looks like ")
	auxfunctions.PrintStringArrayContents(names)
	fmt.Println("Name at the 1st position (starting from 0) -> ", *secondName)
}
