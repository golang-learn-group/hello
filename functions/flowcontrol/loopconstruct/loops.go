package loopconstruct

import (
	"fmt"
)

func ConstructingForLoop() {
	counter := 0
	for {
		fmt.Println("Counter Value ", counter)
		counter++
		if counter > 3 {
			break
		}
	}
}

func BasicForLoop() {
	for counter := 0; counter < 3; counter++ {
		fmt.Println("Counter Value ", counter)
	}
}

func BasicEnumeratingSequencesDemo() {
	arrStr := [4]string{"mba", "engineering", "medical", "armed Forces"}
	for index, str := range arrStr {
		fmt.Println(str, " found in the index ", index)
	}
}

func EnumeratingStringIntoCharactersDemo() {
	str := "Kayak"
	for _, chars := range str {
		fmt.Print(" ", string(chars))
	}
	fmt.Println()
}
