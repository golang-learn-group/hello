package conditionalFlowControl

import (
	"fmt"
	"strconv"
)

const (
	kayakPrice  = 275.00
	priceString = "275"
)

func SimpleIfStatement() {
	if kayakPrice >= 100 {
		fmt.Println("Price is greater than 100")
	}
}

func SimpleIfElseStatement() {
	if kayakPrice > 500 {
		fmt.Println("Price is greater than 500")
	} else if kayakPrice > 200 && kayakPrice < 300 {
		fmt.Println("Price is between 200 and 300")
	}
}

func SimpleIfElseAndElseStatement() {
	if kayakPrice > 500 {
		fmt.Println("Price is greater than 500")
	} else if kayakPrice < 100 {
		fmt.Println("Price is between 200 and 300")
	} else {
		fmt.Println("Price not matched by earlier expressions")
	}
}

func UnderstandingIfElseStatementScope() {
	if kayakPrice > 500 {
		scopedVar := 500
		fmt.Println("Price is greater than ", scopedVar)
	} else if kayakPrice < 100 {
		scopedVar := "Price is less than 100"
		fmt.Println(scopedVar)
	} else {
		scopedVar := "Price is less than 100"
		fmt.Println(scopedVar)
	}
}

func ConvertStringToFloat() {
	if kayakP, err := strconv.Atoi(priceString); err == nil {
		fmt.Println("kayak Price ", kayakP)
	} else {
		fmt.Println(err)
	}
}
