package switchcase

import "fmt"

func BasicSwitchCase() {
	product := "kayak"

	for index, character := range product {
		switch character {
		case 'K' | 'k':
			fmt.Println("K at position ", index)
		case 'Y' | 'y':
			fmt.Println("Y/y at position", index)
		}
	}
}
